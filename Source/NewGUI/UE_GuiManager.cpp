// Fill out your copyright notice in the Description page of Project Settings.


#include "UE_GuiManager.h"

// Sets default values
AUE_GuiManager::AUE_GuiManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AUE_GuiManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AUE_GuiManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FString AUE_GuiManager::Browse()
{
	UWorld* gwengine = GetWorld();// GetGameWorld();
	if (gwengine)
	{
		if (gwengine->GetGameViewport())
		{
			void* ParentWindowHandle = gwengine->GetGameViewport()->GetWindow()->GetNativeWindow()->GetOSWindowHandle();
			IDesktopPlatform* DesktopPlatform = FDesktopPlatformModule::Get();
			if (DesktopPlatform)
			{
				//Opening the file picker!
				uint32 SelectionFlag = 0; //A value of 0 represents single file selection while a value of 1 represents multiple file selection
				TArray<FString> OutFileNames;

				bool isSuccess = DesktopPlatform->OpenFileDialog(ParentWindowHandle, FString("Choose Image"), FString("C:/"), FString(""), "", SelectionFlag, OutFileNames);
				if (isSuccess && OutFileNames.Num() > 0)
				{
					DEBUG("This is the name", *OutFileNames[0]);
					return OutFileNames[0];
				}
			}
		}
	}
	return "";
}

void AUE_GuiManager::Export(FString exportPath, FString subName, FString extension)
{
	FString fileName = "";
	extension = "." + extension;
	fileName = subName + "_AO";
	FString completePath = exportPath + "/" + subName + "/" + fileName;
	//this->EventExportTarget(rt_AO_B, FName(*completePath), FName(*fileName));
}

void AUE_GuiManager::ImportFileAsRaw(const FString& Filename, UTextureCube* NewTexture)
{
	IImageWrapperModule& ImageWrapperModule = FModuleManager::Get().LoadModuleChecked<IImageWrapperModule>(TEXT("ImageWrapper"));
	

	TArray<uint8> Buffer;
	if (FFileHelper::LoadFileToArray(Buffer, *Filename))
	{
		EPixelFormat PixelFormat = PF_Unknown;
		uint8* RawData = nullptr;
		int32 BitDepth = 0;
		int32 Width = 0;
		int32 Height = 0;
		DEBUG("Reaching here 0");
		if (FPaths::GetExtension(Filename) == TEXT("HDR"))
		{
			DEBUG("Reaching here 1");
			FHDRLoadHelper HDRLoadHelper(Buffer.GetData(), Buffer.GetAllocatedSize());
			if (HDRLoadHelper.IsValid())
			{
				DEBUG("Reaching here 2");
				TArray<uint8> DDSFile;
				HDRLoadHelper.ExtractDDSInRGBE(DDSFile);
				FDDSLoadHelper HDRDDSLoadHelper(DDSFile.GetData(), DDSFile.Num());
				DEBUG("%d,%d", HDRLoadHelper.GetWidth(), HDRLoadHelper.GetHeight());



				PixelFormat = HDRDDSLoadHelper.ComputePixelFormat();
				Width = HDRDDSLoadHelper.DDSHeader->dwWidth;
				Height = HDRDDSLoadHelper.DDSHeader->dwHeight;


				DEBUG("Reaching here 3");
				if (NewTexture)
				{
					TextureCompressionSettings oldCompression = NewTexture->CompressionSettings;
					//	TextureMipGenSettings oldMipSetting = NewTexture->MipGenSettings;
					uint8 oldSRGB = NewTexture->SRGB;

					NewTexture->CompressionSettings = TextureCompressionSettings::TC_VectorDisplacementmap;
					//	NewTexture->MipGenSettings = TextureMipGenSettings::TMGS_NoMipmaps;
					NewTexture->SRGB = true;
					NewTexture->UpdateResource();

					void*  MipData = (NewTexture->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE));

					DEBUG("Reaching here x");

					// Bulk data was already allocated for the correct size when we called CreateTransient above
					TArray<FColor> *rawData = new TArray<FColor>();
					for (int i = 0; i < 201326592; i++)
					{

						FColor pixel1 = FColor(125, 255, 0, 0);
						rawData->Add(pixel1);
					}


					FMemory::Memcpy(MipData, rawData->GetData(), 201326592);
					DEBUG("Reaching herey");


					//	NewTexture->MipGenSettings = oldMipSetting;


					NewTexture->PlatformData->Mips[0].BulkData.Unlock();
					NewTexture->UpdateResource();
				}


			}
		}

	}
}

void AUE_GuiManager::GenerateTexture(const FString& Filename,UTextureCube* toTest)
{

	IImageWrapperModule& ImageWrapperModule = FModuleManager::Get().LoadModuleChecked<IImageWrapperModule>(TEXT("ImageWrapper"));


	TArray<uint8> Buffer;
	if (FFileHelper::LoadFileToArray(Buffer, *Filename))
	{
		EPixelFormat PixelFormat = PF_Unknown;
		uint8* RawData = nullptr;
		int32 BitDepth = 0;
		int32 Width = 0;
		int32 Height = 0;
		DEBUG("Reaching here 0");
		if (FPaths::GetExtension(Filename) == TEXT("HDR"))
		{
			DEBUG("Reaching here 1");
			FHDRLoadHelper HDRLoadHelper(Buffer.GetData(), Buffer.GetAllocatedSize());
			if (HDRLoadHelper.IsValid())
			{
				DEBUG("Reaching here 2");
				TArray<uint8> DDSFile;
				HDRLoadHelper.ExtractDDSInRGBE(DDSFile);
				FDDSLoadHelper HDRDDSLoadHelper(DDSFile.GetData(), DDSFile.Num());
				



				PixelFormat = HDRDDSLoadHelper.ComputePixelFormat();
				Width = HDRDDSLoadHelper.DDSHeader->dwWidth;
				Height = HDRDDSLoadHelper.DDSHeader->dwHeight;

			
				//Create a string containing the texture's path
				FString PackageName = TEXT("/Game/Proc/");
				FString BaseTextureName = FString("ProcTexture");
				PackageName += BaseTextureName;

				//Create the package that will store our texture
				UPackage* Package = CreatePackage(NULL, *PackageName);
				GLog->Log("project dir:" + FPaths::ProjectDir());

				//Create a unique name for our asset. For example, if a texture named ProcTexture already exists the editor
				//will name the new texture as "ProcTexture_1"
				FName TextureName = MakeUniqueObjectName(Package, UTexture2D::StaticClass(), FName(*BaseTextureName));
				Package->FullyLoad();

				UTextureCube* NewTexture = NewObject<UTextureCube>(Package, TextureName, RF_Public | RF_Standalone | RF_MarkAsRootSet);

				if (NewTexture)
				{
					int32 TextureWidth = Width;
					int32 TextureHeight = Height;

					//Prevent the object and all its descedants from being deleted during garbage collecion
					NewTexture->AddToRoot();

					//Initialize the platform data to store necessary information regarding our texture asset
					NewTexture->PlatformData = new FTexturePlatformData();
					//Width height change
					NewTexture->PlatformData->SizeX = TextureWidth;
					NewTexture->PlatformData->SizeY = TextureHeight;
					//Num slices is 2147483654
					NewTexture->PlatformData->SetNumSlices(6);
					//Float RGBA
					NewTexture->PlatformData->PixelFormat = EPixelFormat::PF_FloatRGBA;
					NewTexture->CompressionSettings = TextureCompressionSettings::TC_HDR;

					//uint8* Pixels = GeneratePixels(TextureHeight, TextureWidth);
					//Each element of the array contains a single color so in order to represent information in
					//RGBA we need to have an array of length TextureWidth * TextureHeight * 4
					uint8* Pixels = new uint8[TextureWidth * TextureHeight * 4];
					const uint8* pixelData = HDRDDSLoadHelper.GetDDSDataPointer();

					for (int32 i = 0; i < TextureHeight*TextureWidth * 4; i=i+4)
					{
						
						if (i >= (TextureHeight*TextureWidth * 4))
						{
							break;
						}
						const uint8* currentIndex = pixelData + i;
						uint8 red =*(currentIndex + 2);
						uint8 green = *(currentIndex + 1);
						uint8 blue = *currentIndex;
						uint8 alpha = *(currentIndex + 3);
						FColor RandomColor = FColor(red, green, blue, alpha);

						Pixels[i] = RandomColor.B*2; //b
						Pixels[i+1] = RandomColor.G*2; //g
						Pixels[i+2] = RandomColor.R*2; //r
						Pixels[i+3] = 255;
					}

					//for (int32 y = 0; y < TextureHeight; y++)
					//{
					//	for (int32 x = 0; x < TextureWidth; x++)
					//	{
					//		//Get the current pixel
					//		int32 CurrentPixelIndex = ((y * TextureWidth) + x);

					//		//Get a random vector that will represent the RGB values for the current pixel
					//		FColor RandomColor = FColor::MakeRandomColor();

					//		Pixels[4 * CurrentPixelIndex] = RandomColor.B; //b
					//		Pixels[4 * CurrentPixelIndex + 1] = RandomColor.G; //g
					//		Pixels[4 * CurrentPixelIndex + 2] = RandomColor.R; //r
					//		Pixels[4 * CurrentPixelIndex + 3] = 255; //set A channel always to maximum
					//	}
					//}

					//Allocate first mipmap.
					FTexture2DMipMap* Mip = new FTexture2DMipMap();
					NewTexture->PlatformData->Mips.Add(Mip);
					Mip->SizeX = TextureWidth;
					Mip->SizeY = TextureHeight;

					//Lock the mipmap data so it can be modified
					Mip->BulkData.Lock(LOCK_READ_WRITE);
					
					uint8* TextureData = (uint8*)Mip->BulkData.Realloc(TextureWidth * TextureHeight * 4);
					//Copy the pixel data into the Texture data
					FMemory::Memcpy(TextureData, Pixels, sizeof(uint8) * TextureHeight * TextureWidth * 4);
					Mip->BulkData.Unlock();


					//Initialize a new texture
					NewTexture->Source.Init(TextureWidth, TextureHeight, 1, 1, ETextureSourceFormat::TSF_BGRA8, Pixels);
					NewTexture->UpdateResource();

					//Mark the package as dirty so the editor will prompt you to save the file if you haven't
					Package->MarkPackageDirty();

					//Notify the editor that we created a new asset
					FAssetRegistryModule::AssetCreated(NewTexture);

					//Auto-save the new  asset
					FString PackageFileName = FPackageName::LongPackageNameToFilename(PackageName, FPackageName::GetAssetPackageExtension());
					bool bSaved = UPackage::SavePackage(Package, NewTexture, EObjectFlags::RF_Public | EObjectFlags::RF_Standalone, *PackageFileName, GError, nullptr, true, true, SAVE_NoError);

					//Since we don't need access to the pixel data anymore free the memory
					delete[] Pixels;
				}
			}
		}
	}
}