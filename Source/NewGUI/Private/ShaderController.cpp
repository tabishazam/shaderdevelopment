// Fill out your copyright notice in the Description page of Project Settings.


#include "ShaderController.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMeshActor.h" 
// Sets default values
AShaderController::AShaderController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AShaderController::SetRenderTarget(UTextureRenderTarget2D* rt)
{
	RenderTarget = rt;
}

// Called when the game starts or when spawned
void AShaderController::BeginPlay()
{
	Super::BeginPlay();
	_shaderModule = new UShaderModule();
	_shaderModule->BeginRendering();

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AStaticMeshActor::StaticClass(), FoundActors);

	for (AActor* actor : FoundActors)
	{
		FString name;
		actor->GetName(name);
		if (name.Equals("Cube_2"))
		{
			solidMix =actor->FindComponentByClass<UStaticMeshComponent>();
			
			break;
		}
	}

	
	if (solidMix != nullptr)
	{
		
		if (dynamicMaterial == nullptr)
		{
			
			dynamicMaterial = UMaterialInstanceDynamic::Create(material, solidMix);
			dynamicMaterial->SetTextureParameterValue("CustomTexture", RenderTarget);
			solidMix->SetMaterial(0, dynamicMaterial);
		}
	}
}




void AShaderController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	_shaderModule->EndRendering();
}

// Called every frame
void AShaderController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FShaderParamsCustom DrawParameters(RenderTarget);
	_shaderModule->UpdateParameters(DrawParameters);
}

