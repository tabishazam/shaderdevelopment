// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "NewGUIGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class NEWGUI_API ANewGUIGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
