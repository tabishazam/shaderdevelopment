// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
using System.IO;
using System; // Console.WriteLine("");

using UnrealBuildTool;

public class NewGUI : ModuleRules
{
    private string ModulePath
    {
        get { return ModuleDirectory; }
    }

    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModulePath, "../../ThirdParty/")); }
    }

    public NewGUI(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] {"Core",
            "CoreUObject",
            "Engine",
            "InputCore",
             "RHI",
            "Slate" ,
          });
        PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore",  "Renderer",
            "RenderCore"});
        PrivateDependencyModuleNames.AddRange(new string[] { });
     
        // Uncomment if you are using Slate UI

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");

        // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
    }

  

    string[] FindAllFiles(string path)
    {
        string[] files = Directory.GetFiles(path, "*.lib", SearchOption.AllDirectories);
        return files;
    }
}
