// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "Async/Async.h" 
#include "Engine/GameEngine.h" 
#include "Engine/TextureCube.h" 
#include "Engine.h"
#include "Misc/FileHelper.h"
#include "HDRLoader.h" 
#include "DDSLoader.h" 
#include "IImageWrapperModule.h"
#include "AssetRegistryModule.h" 
#include "Engine/Texture.h" 
#include "Modules/ModuleManager.h" 
#include "Developer/DesktopPlatform/Public/IDesktopPlatform.h"
#include "Developer/DesktopPlatform/Public/DesktopPlatformModule.h"
#include "GenericPlatform/GenericPlatformMisc.h" 
#include "GameFramework/Actor.h"
#include "UE_GuiManager.generated.h"

#define DEBUG(arg_,...) UE_LOG(LogTemp,Warning,TEXT(arg_),__VA_ARGS__)

UCLASS()
class NEWGUI_API AUE_GuiManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUE_GuiManager();
	UFUNCTION(BlueprintCallable, Category = Setup)
		void GenerateTexture(const FString& FilenameX, UTextureCube* toTest);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable, Category = Setup)
		FString Browse();
	void Export(FString exportPath, FString subName, FString extension);
	UFUNCTION(BlueprintCallable, Category = Setup)
	void ImportFileAsRaw(const FString& Filename, UTextureCube* NewTexture);

	UFUNCTION(BlueprintImplementableEvent, Category = Operation)
		void EventExportTarget(UTextureRenderTarget2D* RT_ToExport, FName pathToExport, FName fileName);
};
