// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ShaderModule.h"
#include "ShaderController.generated.h"


UCLASS()
class NEWGUI_API AShaderController : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShaderController();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ShaderDemo)
	UTextureRenderTarget2D* RenderTarget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ShaderDemo)
	UStaticMeshComponent* solidMix = nullptr;

	UPROPERTY(EditAnywhere)
		UMaterialInterface* material;

	UMaterialInstanceDynamic* dynamicMaterial;

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void SetRenderTarget(UTextureRenderTarget2D* rt);
private:
	UShaderModule* _shaderModule;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
