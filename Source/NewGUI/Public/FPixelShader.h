// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShaderModule.h"
/**
 * 
 */
struct FShaderParamsCustom;
class FPixelShader
{
public:
	static void DrawToRenderTarget_RenderThread(FRHICommandListImmediate& RHICmdList, const FShaderParamsCustom& DrawParameters, FTextureRHIRef ComputeShaderOutput);
};
