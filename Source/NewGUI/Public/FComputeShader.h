// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShaderModule.h"
/**
 * 
 */
struct FShaderParamsCustom;
class FComputeShader
{
public:
	static void RunComputeShader_RenderThread(FRHICommandListImmediate& RHICmdList, const FShaderParamsCustom& DrawParameters, FUnorderedAccessViewRHIRef ComputeShaderOutputUAV);
};