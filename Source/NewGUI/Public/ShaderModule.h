// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"
#include "RenderGraphResources.h"
#include "Runtime/Engine/Classes/Engine/TextureRenderTarget2D.h"
#include "FComputeShader.h"
#include "FPixelShader.h"


struct FShaderParamsCustom
{
	UTextureRenderTarget2D* RenderTarget;

	FShaderParamsCustom() { }
	FShaderParamsCustom(UTextureRenderTarget2D* InRenderTarget) : RenderTarget(InRenderTarget)
	{
	}
};

/**
 * 
 */

class UShaderModule
{
	
public:
		void BeginRendering();
		void EndRendering();
		void UpdateParameters(FShaderParamsCustom& DrawParameters);
private:
	void PostResolveSceneColor_RenderThread(FRHICommandListImmediate& RHICmdList, class FSceneRenderTargets& SceneContext);
	void Draw_RenderThread(const FShaderParamsCustom& DrawParameters);
	TRefCountPtr<IPooledRenderTarget> ComputeShaderOutput;
	FDelegateHandle OnPostResolvedSceneColorHandle;
	FCriticalSection RenderEveryFrameLock;
	FShaderParamsCustom CachedShaderUsageExampleParameters;
	volatile bool bCachedParametersValid;
};
