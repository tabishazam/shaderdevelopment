// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "NewGUI.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FNewGUIModule, NewGUI, "NewGUI" );

void FNewGUIModule::StartupModule()
{
	FString ShaderDirectory = FPaths::Combine(FPaths::ProjectDir(), TEXT("Shaders"));
	AddShaderSourceDirectoryMapping("/Project", ShaderDirectory);
}

void FNewGUIModule::ShutdownModule()
{
	ResetAllShaderSourceDirectoryMappings();
}
